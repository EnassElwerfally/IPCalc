package fr.jnda.ipcalc.network

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject


class NetworkRequest(context: Context) {
    private val factory: VolleyHelper = VolleyHelper.getInstance(context)
    private val apiUrl = "https://api.ipgeolocation.io/ipgeo?apiKey=4eea10dfa2a44e0fbd831400b3bbcf9a&ip="
    private val mListeners = mutableListOf<NetworkCallback>()

    fun addRequest(address: String) {
        factory.addToRequestQueue(buildRequest(address))
    }

    private fun buildRequest(address: String): JsonObjectRequest {
        return JsonObjectRequest(Request.Method.GET, apiUrl + address, null,
                Response.Listener { response ->
                    fireSuccess(response)
                },
                Response.ErrorListener { error ->
                    val errorCode = error?.networkResponse?.statusCode?:-1
                    val a = error.networkResponse?.data?: byteArrayOf()
                    val responseCode = String(a)

                    fireError(errorCode,JSONObject(responseCode))
                })

    }

    interface NetworkCallback{
        fun onSuccess(response: JSONObject)
        fun onError(erroCode: Int, response: JSONObject)
    }

    fun addNetworkCallbackListener(listener: NetworkCallback){
        if(!mListeners.contains(listener)) {
            mListeners.add(listener)
        }
    }


    fun delNetworkCallbackListener(listener: NetworkCallback){
        if(mListeners.contains(listener))
            mListeners.remove(listener)
    }

    private fun fireSuccess(response: JSONObject){
        mListeners.forEach {
            it.onSuccess(response)
        }
    }

    private fun fireError(erroCode: Int, response: JSONObject){
        mListeners.forEach {
            it.onError(erroCode,response)
        }
    }
}
