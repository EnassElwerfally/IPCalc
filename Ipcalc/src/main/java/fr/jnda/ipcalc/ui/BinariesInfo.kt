package fr.jnda.ipcalc.ui


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import fr.jnda.ipcalc.MainActivity
import fr.jnda.ipcalc.R
import fr.jnda.ipcalc.helper.toSpannedText
import kotlinx.android.synthetic.main.fragment_binaries_info.view.*
import java.util.*


class BinariesInfo : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_binaries_info, container, false)
        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity){
            mainActivity = context as MainActivity
        }
    }

    override fun displayInfo(cidr4: Cidr4, address: String) {

        rootView.id_ip_address.text = address.toBinariesString().toSpannedText(getString(R.string.address_ip))
        rootView.id_subnet_address.text = cidr4.netmask.toString().toBinariesString().toSpannedText(getString(R.string.address_subnet))
        rootView.id_network_address.text = cidr4.cidrSignature.cidrSignatureToIp().toBinariesString().toSpannedText(getString(R.string.address_network))
        rootView.id_broadcast_address.text = cidr4.getHighAddress(true).toBinariesString().toSpannedText(getString(R.string.address_broadcast))
        rootView.id_host_min.text = cidr4.getLowAddress(false).toBinariesString().toSpannedText(getString(R.string.address_hostmin))
        rootView.id_host_max.text = cidr4.getHighAddress(false).toString().toBinariesString().toSpannedText(getString(R.string.address_hostmax))
    }

    private fun String.toBinariesString() :String {
        val ip = Ip4(this)
        val builder = StringBuilder()
        for (b in ip.inetAddress.address) {
            builder.append(String.format(Locale.FRANCE,"%08d", Integer.toBinaryString(b.toInt().and(0xff)).toInt())).append(" . ")
        }
        return builder.deleteCharAt(builder.length-2).toString()
    }


    override fun clearInfo(){
        rootView.id_ip_address.text = ""
        rootView.id_subnet_address.text = ""
        rootView.id_network_address.text = ""
        rootView.id_broadcast_address.text = ""
        rootView.id_host_min.text = ""
        rootView.id_host_max.text = ""
    }

    private fun String.cidrSignatureToIp():String{
        return this.substring(0,this.indexOf("/"))
    }
}
