package fr.jnda.ipcalc.ui


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri.parse
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.github.veqryn.net.Cidr4
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import fr.jnda.ipcalc.R
import fr.jnda.ipcalc.helper.toSpannedText
import fr.jnda.ipcalc.network.NetworkRequest
import fr.jnda.ipcalc.poko.GeolocalisationResponse
import kotlinx.android.synthetic.main.fragment_location_info.*
import kotlinx.android.synthetic.main.fragment_location_info.view.*
import org.json.JSONException
import org.json.JSONObject


class LocationInfo : BaseFragment(),NetworkRequest.NetworkCallback {

    private lateinit var networkRequest: NetworkRequest
    private lateinit var rootView: View
    private lateinit var mContext: Context

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.fragment_location_info, container, false)
        return rootView
    }

    override fun displayInfo(cidr4: Cidr4, address: String) {
        if (isConnected()) {
            showProgress(true)
            networkRequest.addRequest(address)
        }
        else {
            Snackbar.make(rootView, getString(R.string.info_not_connected), Snackbar.LENGTH_LONG).show()
            clearInfo()
        }
    }

    private fun showProgress(state:Boolean){
        progressBar.visibility = if(state) View.VISIBLE else View.GONE
    }

    private fun isConnected(): Boolean{
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    override fun clearInfo() {

        rootView.id_country_flags.setImageResource(android.R.color.transparent)
        rootView.id_location_ip.text = ""
        rootView.id_location_continent_name.text = ""
        rootView.id_location_country_code.text = ""
        rootView.id_location_country_name.text = ""
        rootView.id_location_country_capital.text = ""
        rootView.id_location_province.text = ""
        rootView.id_location_organization.text = ""
        rootView.id_location_isp.text = ""
        rootView.id_location_zipcode.text = ""
        rootView.id_location_city.text = ""
        rootView.id_location_district.text = ""
        rootView.id_location_map.visibility = View.GONE
    }

    private fun showResponse(location: GeolocalisationResponse){
        location.run {
            if(country_flag.isNotEmpty()) {
                Glide.with(this@LocationInfo).load(location.country_flag).into(rootView.id_country_flags)
            }
            if(ip.isNotEmpty()){
                rootView.id_location_ip.text = ip
            }
            if(continent_name.isNotEmpty()){
                rootView.id_location_continent_name.visibility = View.VISIBLE
                val continent = continent_name + if(continent_code.isNotEmpty()) " ( $continent_code )" else ""
                rootView.id_location_continent_name.text = continent.toSpannedText(getString(R.string.location_continent))
            } else {
                rootView.id_location_continent_name.visibility = View.GONE
            }
            if(country_code2.isNotEmpty()){
                rootView.id_location_country_code.visibility = View.VISIBLE
                rootView.id_location_country_code.text = country_code2.toSpannedText(getString(R.string.location_code))
            }else {
                rootView.id_location_country_code.visibility = View.GONE
            }
            if (country_name.isNotEmpty()){
                rootView.id_location_country_name.visibility = View.VISIBLE
                rootView.id_location_country_name.text = country_name.toSpannedText(getString(R.string.location_name))
            } else {
                rootView.id_location_country_name.visibility = View.GONE
            }
            if (country_capital.isNotEmpty()){
                rootView.id_location_country_capital.visibility = View.VISIBLE
                rootView.id_location_country_capital.text = country_capital.toSpannedText(getString(R.string.location_capital))
            } else {
                rootView.id_location_country_capital.visibility = View.GONE
            }
            if (state_prov.isNotEmpty()){
                rootView.id_location_province.visibility = View.VISIBLE
                rootView.id_location_province.text = state_prov.toSpannedText(getString(R.string.location_province))
            } else {
                rootView.id_location_province.visibility = View.GONE
            }
            if (district.isNotEmpty()){
                rootView.id_location_district.visibility = View.VISIBLE
                rootView.id_location_district.text = district.toSpannedText(getString(R.string.location_district))
            } else {
                rootView.id_location_district.visibility = View.GONE
            }
            if (city.isNotEmpty()){
                rootView.id_location_city.visibility = View.VISIBLE
                rootView.id_location_city.text = city.toSpannedText(getString(R.string.location_city))
            } else {
                rootView.id_location_city.visibility = View.GONE
            }
            if (zipcode.isNotEmpty()){
                rootView.id_location_zipcode.visibility = View.VISIBLE
                rootView.id_location_zipcode.text = zipcode.toSpannedText(getString(R.string.location_zipcode))
            } else {
                rootView.id_location_zipcode.visibility = View.GONE
            }
            if (isp.isNotEmpty()){
                rootView.id_location_isp.visibility = View.VISIBLE
                rootView.id_location_isp.text = isp.toSpannedText(getString(R.string.location_isp))
            } else {
                rootView.id_location_isp.visibility = View.GONE
            }
            if (organization.isNotEmpty()){
                rootView.id_location_organization.visibility = View.VISIBLE
                rootView.id_location_organization.text = organization.toSpannedText(getString(R.string.location_organisation))
            } else {
                rootView.id_location_organization.visibility = View.GONE
            }
            if (latitude.isNotEmpty() && longitude.isNotEmpty()){
                rootView.id_location_map.visibility = View.VISIBLE
                rootView.id_location_map.setOnClickListener {
                    val gmmIntentUri = parse("geo:$latitude,$longitude")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    if (mapIntent.resolveActivity(mContext.packageManager) != null) {
                        startActivity(mapIntent)
                    }else{
                        Snackbar.make(rootView, R.string.no_gmaps, Snackbar.LENGTH_SHORT).show()
                    }
                }
            } else {
                rootView.id_location_map.visibility = View.GONE
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        networkRequest = NetworkRequest(context)
        networkRequest.addNetworkCallbackListener(this)
    }

    override fun onDetach() {
        super.onDetach()
        networkRequest.delNetworkCallbackListener(this)
    }

    override fun onSuccess(response: JSONObject) {
        showProgress(false)
        val locationResponse = Gson().fromJson(response.toString(), GeolocalisationResponse::class.java)
        Log.d("Location","Response $locationResponse")
        showResponse(locationResponse)
    }

    override fun onError(erroCode: Int, response: JSONObject) {
        clearInfo()
        showProgress(false)
        val message:String = try {
            response.get("message").toString()
        } catch (e: JSONException){
            getString(R.string.error_api)
        }
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
        Log.d("Location","Erreur $erroCode -- $response")
    }
}
