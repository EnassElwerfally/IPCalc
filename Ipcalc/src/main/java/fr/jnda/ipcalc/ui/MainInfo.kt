package fr.jnda.ipcalc.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import fr.jnda.ipcalc.R
import fr.jnda.ipcalc.helper.toSpannedText
import kotlinx.android.synthetic.main.main_info.view.*
import java.net.UnknownHostException


class MainInfo : BaseFragment() {

    private lateinit var rootView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.main_info, container, false)
        clearInfo()
        return rootView
    }

    override fun displayInfo(cidr4: Cidr4,address: String){

        rootView.id_ip_address.text =  address.toSpannedText(getString(R.string.address_ip))
        rootView.id_subnet_address.text = cidr4.netmask.toString().toSpannedText(getString(R.string.address_subnet))
        rootView.id_network_address.text = cidr4.cidrSignature.toString().toSpannedText(getString(R.string.address_network))
        rootView.id_broadcast_address.text = cidr4.getHighAddress(true).toSpannedText(getString(R.string.address_broadcast))
        rootView.id_host_min.text = cidr4.getLowAddress(false).toSpannedText(getString(R.string.address_hostmin))
        rootView.id_host_max.text = cidr4.getHighAddress(false).toSpannedText(getString(R.string.address_hostmax))
        rootView.id_host_count.text = cidr4.getAddressCount(false).toString().toSpannedText(getString(R.string.address_hostcount))
        rootView.id_address_class.text = getClassOfIp(address).toSpannedText(getString(R.string.address_class))
    }

    override fun clearInfo(){

        rootView.id_ip_address.text = ""
        rootView.id_subnet_address.text = ""
        rootView.id_network_address.text = ""
        rootView.id_broadcast_address.text =""
        rootView.id_host_min.text = ""
        rootView.id_host_max.text = ""
        rootView.id_host_count.text = ""
        rootView.id_address_class.text = ""
    }

    private fun getClassOfIp(ip: String): String {
        //  displayBinaryAddress(ip)
        val classeA = Cidr4("10.0.0.0/8")
        val classeB = Cidr4("172.16.0.0/12")
        val classeC = Cidr4("192.168.0.0/16")
        val classeLo = Cidr4("127.0.0.0/8")

        val myIP1 = Ip4(ip)

        when {
            classeA.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivA)
            classeB.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivB)
            classeC.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivC)
            classeLo.isInRange(myIP1, true) -> return getString(R.string.lbl_classeLo)
            else -> {
                try {
                    val b = myIP1.inetAddress.address

                    val s1 = String.format("%8s", Integer.toBinaryString(b[0].toInt().and(0xff))).replace(' ', '0')
                    when {
                        s1.startsWith("0") -> return getString(R.string.lbl_classeA)
                        s1.startsWith("10") -> return getString(R.string.lbl_classeB)
                        s1.startsWith("110") -> return getString(R.string.lbl_classeC)
                        s1.startsWith("1110") -> return getString(R.string.lbl_classeD)
                        s1.startsWith("1111") -> return getString(R.string.lbl_classeE)
                    }
                } catch (e: UnknownHostException) {
                    e.printStackTrace()
                }
                return getString(R.string.lbl_indetermine)
            }
        }
    }
}