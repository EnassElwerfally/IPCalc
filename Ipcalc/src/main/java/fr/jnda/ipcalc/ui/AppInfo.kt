package fr.jnda.ipcalc.ui


import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import fr.jnda.ipcalc.MainActivity


/**
 * A simple [Fragment] subclass.
 */
class AppInfo : PreferenceFragmentCompat() {

    private lateinit var mContext: Context
    private lateinit var mActivity: MainActivity

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(fr.jnda.ipcalc.R.xml.appinfo)
        findPreference<Preference>("pref_key_version_name")?.apply {
            try {
                val pInfo = context.packageManager.getPackageInfo(mContext.packageName, 0)
                summary = pInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                summary = "-"
                e.printStackTrace()
            }

        }

        findPreference<Preference>("pref_key_version_code")?.apply {
            try {
                val pInfo = context.packageManager.getPackageInfo(mContext.packageName, 0)
                summary = pInfo.versionCode.toString()
            } catch (e: PackageManager.NameNotFoundException) {
                summary = "-"
                e.printStackTrace()
            }
        }

        findPreference<Preference>("pref_key_issues")?.apply {
            summary = """https://gitlab.com/jnda/IPCalc/issues"""
            this.setOnPreferenceClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse("https://gitlab.com/jnda/IPCalc/issues")
                startActivity(i)
                true
            }
        }

        findPreference<SwitchPreference>("pref_key_dark_style")?.apply {
            setOnPreferenceChangeListener { _, bool ->
                if (bool as Boolean)
                    mActivity.delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_YES
                else
                    mActivity.delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_NO
                true
            }

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        mActivity = mContext as MainActivity

    }
}
