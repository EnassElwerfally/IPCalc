package fr.jnda.ipcalc.helper

import android.os.Build
import android.text.Html
import android.text.Spanned

@Suppress("DEPRECATION")
fun String.toSpannedText(text:String): Spanned{
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ->
            Html.fromHtml(String.format(text,"<br/>&nbsp;&nbsp;&nbsp;$this"), Html.FROM_HTML_MODE_LEGACY)
        else -> Html.fromHtml(String.format(text, "<br/>&nbsp;&nbsp;&nbsp;$this"))
    }
}