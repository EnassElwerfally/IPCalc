package fr.jnda.ipcalc.poko

data class GeolocalisationResponse(
        val ip: String,
        val continent_code: String,
        val continent_name: String,
        val country_code2: String,
        val country_code3: String,
        val country_name: String,
        val country_capital: String,
        val state_prov: String,
        val district: String,
        val city: String,
        val zipcode: String,
        val latitude: String,
        val longitude: String,
        val country_flag: String,
        val isp: String,
        val organization: String)