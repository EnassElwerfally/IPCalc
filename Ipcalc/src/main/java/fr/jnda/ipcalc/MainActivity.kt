package fr.jnda.ipcalc

import android.app.Activity
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.jnda.ipcalc.helper.IPV4_PATTERN
import fr.jnda.ipcalc.helper.IPV6_STD_PATTERN
import fr.jnda.ipcalc.ui.*
import kotlinx.android.synthetic.main.activity_v2.*


class MainActivity :AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isNightMode = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_key_dark_style",false)

        delegate.localNightMode = if (isNightMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO

        setContentView(R.layout.activity_v2)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        navView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_info -> {     supportFragmentManager.beginTransaction().replace(R.id.id_main_content, MainInfo()).commitNowAllowingStateLoss()}
                R.id.navigation_binaries -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, BinariesInfo()).commitNowAllowingStateLoss() }
                R.id.navigation_location -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, LocationInfo()).commitNowAllowingStateLoss() }
                R.id.navigation_settings -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, AppInfo()).commitNowAllowingStateLoss() }
            }
            hideKeyboard(this@MainActivity)
            displayInfo()

            true
        }

        seekBar2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                displayInfo()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })
        id_ip_address.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                displayInfo()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        subnet_help.helperText = String.format(getString(R.string.lbl_subnet),"","")
        supportFragmentManager.beginTransaction().replace(R.id.id_main_content, MainInfo()).commitNow()

    }



    private fun displayInfo(){
        val address = id_ip_address.text.toString()
        val fragment = supportFragmentManager.findFragmentById(R.id.id_main_content)

        when {
            IPV4_PATTERN.matcher(address).matches() -> {
                subnet_help.helperText = String.format(getString(R.string.lbl_subnet),getCidr4(address).netmask,seekBar2.progress.toString())
                seekBar2.isEnabled = true

                fragment?.let {
                    if (fragment is BaseFragment)
                        fragment.displayInfo(getCidr4(address),address)
                }
            }
            IPV6_STD_PATTERN.matcher(address).matches() -> {
                subnet_help.helperText = String.format(getString(R.string.lbl_subnet),"","")
                seekBar2.isEnabled = false
                Toast.makeText(this@MainActivity,getString(R.string.ipv6_not_supported),Toast.LENGTH_LONG).show()
            }
            else -> {
                subnet_help.helperText = String.format(getString(R.string.lbl_subnet),"","")
                seekBar2.isEnabled = false
                fragment?.let {
                    if (fragment is BaseFragment)
                        fragment.clearInfo()
                }
            }
        }
    }

    private fun getCidr4(address: String):Cidr4{
        val myIP2 = Ip4(address)
        return myIP2.getLowestContainingCidr(seekBar2.progress)
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = activity.currentFocus?:View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}