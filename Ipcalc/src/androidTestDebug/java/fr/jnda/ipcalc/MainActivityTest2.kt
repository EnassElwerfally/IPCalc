package fr.jnda.ipcalc


import android.Manifest
import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import java.util.*

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest2 {


    @get:Rule
    public var mRuntimePermissionRule = GrantPermissionRule.grant(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.DISABLE_KEYGUARD,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.CHANGE_CONFIGURATION)


    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)


    private fun setLocale(language: String, country: String) {
        val locale = Locale(language, country)
        Locale.setDefault(locale)
        val res = mActivityTestRule.activity.resources
        val config = res.configuration
        config.locale = locale
        res.updateConfiguration(config, res.displayMetrics)
    }

    @Test
    fun runAllTest(){
        setLocale("en", "EN")
        mainTest()
        setLocale("fr", "FR")
        mainTest()
    }


    fun mainTest() {

        val textInputEditText = onView(
                allOf(withId(R.id.id_ip_address),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("com.google.android.material.textfield.TextInputLayout")),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText.perform(replaceText("8.8.8.8"), closeSoftKeyboard())
        Screengrab.screenshot("first_screen")

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.navigation_binaries), withContentDescription("Voir en binaire"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        Screengrab.screenshot("second_screen")
        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.navigation_location), withContentDescription("Localisation"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        Screengrab.screenshot("third_screen")
        val bottomNavigationItemView3 = onView(
                allOf(withId(R.id.navigation_settings), withContentDescription("Paramètres"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                3),
                        isDisplayed()))
        bottomNavigationItemView3.perform(click())
        Screengrab.screenshot("fourth_screen")
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
